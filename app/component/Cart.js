/*
 * @Author: tiero 
 * @Date: 2016-10-01 21:07:25 
 * @Last Modified by: tiero
 * @Last Modified time: 2016-10-02 15:00:03
 */

import React, { Component, PropTypes } from 'react'
import {
    Dimensions,    
    ListView,
    Modal,
    Image,
    View,
    Text,
} from 'react-native'
import { List, ListItem, Button, CheckBox } from 'react-native-elements'
import {storeId} from '../helpers/data'


const {height,width} = Dimensions.get('window')
export default class Cart extends Component {
    
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.dataSource = ds.cloneWithRows([props.product])
        this.state = {
            visa: true,
            mastercard: false,
            modal: false,
        }
        //bind
        this.renderRow = this.renderRow.bind(this)
        this.callBuy = this.callBuy.bind(this)
    }

    callBuy() {
        let prd = JSON.stringify(this.props.product)
        fetch('http://10.36.108.236:3000/buy?storeId='+storeId+'&products=['+prd+']', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        })
     this.setState({modal:true})
    }



    renderRow (rowData, sectionID, rowID) {
        return (
             <ListItem
                roundAvatar
                title={rowData.name}
                subtitle={'$ ' + rowData.price}
            />
        )
    }

    render() {
        return (
            <View style={{marginTop:20}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modal}>
               <View style={{
                    marginTop: 50, 
                    backgroundColor:'rgba(0,0,0,0.9)'}}>
                    <Text style={{color:'white',fontSize:50, textAlign:'center'}}>Payment succesful</Text>
                    <Image source={require('fastApp/app/img/check.png')} style={{marginHorizontal:70,marginVertical:70,height:200,width:200,alignItems:'center'}}/>                     
                    <Button 
                        onPress={() => {
                            this.setState({modal:!this.state.modal})
                        }}
                        small={true}
                        title="close"
                        raised />
               </View> 
            </Modal>
                <View style={{height:50,flexDirection:'row', justifyContent:'flex-start', alignItems:'flex-start'}}>
                    <Button small={true} title={"back"} onPress={()=>this.props.navigator.pop()} />
                    <Text style={{fontSize:30,marginLeft:50}}>Cart</Text>
                </View>
                <Image source={require('fastApp/app/img/qrcode.jpg')} style={{marginHorizontal:70,height:200,width:200,alignItems:'center'}}/>                     
                <View style={{ justifyContent:'center', alignItems:'center',backgroundColor:'lightgrey'}}>
                    <Text style={{fontSize:30}}>TOTAL $ {this.props.product.price}</Text>
                </View>
                <List containerStyle={{height:100}}>
                    <ListView
                        renderRow={this.renderRow}
                        dataSource={this.dataSource}
                    />
                </List>
                <Text style={{marginHorizontal:100}}>Choose the account type </Text>
                <View style={{flexDirection:'row', alignItems:'center'}}>

                    <CheckBox
                        title='Personal'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        onPress={()=>this.setState({
                                        mastercard:false,
                                        visa:true})}
                        checked={this.state.visa} />
                    <CheckBox
                        title='Business'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        onPress={()=>this.setState({
                                        mastercard:true,
                                        visa:false})}
                        checked={this.state.mastercard}
                            />
                    </View>
                <Button
                    raised
                    title='BUY'
                    onPress={()=>this.callBuy()}
                    backgroundColor='darkgreen' />
            </View>)
    }
}

Cart.propTypes = {
    navigator: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
}