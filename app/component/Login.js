/*
 * @Author: tiero 
 * @Date: 2016-10-01 14:44:42 
 * @Last Modified by: tiero
 * @Last Modified time: 2016-10-02 07:17:56
 */

import React, { Component, PropTypes } from 'react'
import {
  StyleSheet,
  Dimensions,
  Image,
  Text,
  View
} from 'react-native'
import { Button, Icon } from 'react-native-elements'

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image 
          source={require('fastApp/app/img/b-pay-home.jpg')}
          style={styles.bgImage} />
        <Text style={styles.welcome}>
          FAST PAY
        </Text>
        <Text style={styles.instructions}>
          Pay fast, pay smarter
        </Text>
        <Button
          raised
          onPress={()=>this.props.navigator.push({title:'products'})}
          backgroundColor="blue"
          title='LOGIN' />
          <View style={{
            marginTop:20,
            backgroundColor:'white',
            borderTopColor:'black',
            borderTopWidth: 1,
            flexDirection:'row',
            justifyContent:'center'
          }}>
          <Icon
            name='home'
            size={37}
            color='#517fa4'
            iconStyle={{marginHorizontal:20}}
          />
          <Icon
            name='accessibility'
            size={37}
            color='#517fa4'
            iconStyle={{marginHorizontal:20}}            
          />
          <Icon
            name='history'
            size={37}
            color='#517fa4'
            onPress={()=>this.props.navigator.push({title:'history'})}            
            iconStyle={{marginHorizontal:20}}            
          />
          <Icon
            name='camera'
            size={37}
            color='#517fa4'
            onPress={()=>this.props.navigator.push({title:'qrcode'})}
            iconStyle={{marginHorizontal:20}}            
          />
          </View>
      </View>
    );
  }
}

Login.propTypes = {
    navigator: PropTypes.object.isRequired
}

const SCREEN_WIDTH = Dimensions.get('window').width
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightskyblue',
  },
  welcome: {
    fontSize: 40,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 25,
  },
  bgImage: {
    alignItems:'flex-start',
    height:300,
    width: SCREEN_WIDTH,
    marginBottom: 40,
  }
});