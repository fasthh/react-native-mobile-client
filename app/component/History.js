/*
 * @Author: tiero 
 * @Date: 2016-10-02 05:09:10 
 * @Last Modified by: tiero
 * @Last Modified time: 2016-10-02 14:27:08
 */
import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions
} from 'react-native'
import { List, ListItem, Button } from 'react-native-elements'

let products = [{name:'iPhone5s',price:500},
                    {name:'iPhone7',price:800}]
const {height,width} = Dimensions.get('window')


export default class History extends Component {

constructor(props) {
    super(props)

    this.state = {
        history: products
    }
}

    componentDidMount() {
        /*fetch('http://10.36.108.236:3000/ourGreatRequest')
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({history:responseJson})
            console.log(responseJson)
            return;
        })
        .catch((error) => {
            console.error(error);
        });*/
    }

    render() {
        return (
            <View style={{marginTop:20}}>
                <View style={{height:50,flexDirection:'row', justifyContent:'flex-start', alignItems:'flex-start'}}>
                    <Button small={true} title={"back"} onPress={()=>this.props.navigator.pop()} />
                    <Text style={{fontSize:20,margin:5}}>Recent payments</Text>
                </View>
                <List containerStyle={{height:height-40, justifyContent: 'flex-start'}}>
                    {
                        this.state.history.map((l, i) => (
                        <ListItem
                            roundAvatar
                            key={''+i}
                            avatar={require('fastApp/app/img/dollar.png')}
                            title={products[i].name}
                            subtitle={'$' + products[i].price}
                        />
                        ))
                    }
                </List>
            </View>)
    }
}