/*
 * @Author: tiero 
 * @Date: 2016-10-01 21:07:25 
 * @Last Modified by: tiero
 * @Last Modified time: 2016-10-02 14:59:12
 */

import React, { Component, PropTypes } from 'react'
import {
    Dimensions,    
    ListView,
    View,
    Text,
} from 'react-native'
import { List, ListItem, SearchBar, Button } from 'react-native-elements'

const products = [
        {name:'iPhone4s',price:199},
        {name:'iPhone7',price:799},
        {name: 'MacBookPro',price:1499}]

const {height,width} = Dimensions.get('window')
export default class Products extends Component {

    constructor(props){
        super(props);
        
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.dataSource = ds.cloneWithRows({})
        this.state = {
            products 
        }
        //bind
        this.renderRow = this.renderRow.bind(this)
    }
    
    renderRow (rowData, sectionID, rowID) {
        var name = rowData.name        
        var avatar = require('../img/iPhone7.png')
        return (
            <ListItem
                roundAvatar
                title={name}
                subtitle={'$' + rowData.price}
                onPress={()=>this.props.navigator.push({title:'cart', passProps: {product:rowData} })}
                rightIcon='chevron-right'
                avatarStyle={{height:128,width:128}}
                avatar={avatar}
            />
        )
    }

    render() {
        return (
            <View style={{marginTop:20}}>
                <SearchBar
                    lightTheme
                    onChangeText={(text)=>null}
                    placeholder='Type Here...' />
                <List containerStyle={{height:height-150, justifyContent:'flex-start'}}>
                <ListView
                    renderRow={this.renderRow}
                    dataSource={this.dataSource.cloneWithRows(this.state.products)}/>
                </List>
                <Button title="Home" small={true} onPress={()=>this.props.navigator.pop()}/>
            </View>)
    }
}

Products.propTypes = {
    navigator: PropTypes.object.isRequired
}