/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
  AppRegistry,
  Navigator
} from 'react-native'
//Components
import Login from './app/component/Login'
import Products from './app/component/Products'
import Cart from './app/component/Cart'
import QRcode from './app/component/QRcode'
import History from './app/component/History'

class fastApp extends Component {

  renderScene(route, navigator) {
    switch (route.title) {
      case 'login':
        return <Login navigator={navigator} {...route.passProps} />
      case 'cart':
        return <Cart navigator={navigator} {...route.passProps}/>
      case 'qrcode':
        return <QRcode navigator={navigator} {...route.passProps} />
      case 'products':
        return <Products navigator={navigator} {...route.passProps} />
      case 'stores':
        return <Stores navigator={navigator} {...route.passProps} />
      case 'history':
        return <History navigator={navigator} {...route.passProps} />
      default:
        return null
    }
  }

  render() {
   return  <Navigator
        initialRoute={{ title: 'login'}}
        renderScene={this.renderScene}
      />
  }
}


AppRegistry.registerComponent('fastApp', () => fastApp);
