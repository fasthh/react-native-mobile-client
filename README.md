# F.A.S.T. Mobile App
### Frictionless Secure Transfer ###


Mobile app for merchants and customers to pay in stores without Credit/Debit Card. 

Built during Hyperledger Hackaton 2016 in Amsterdam

# README #

This README would normally document whatever steps are necessary to get your application up and running.

Install react-native-cli
```
$ npm install -g react-native-cli
```
Clone from master
```sh
$ git clone https://tiero@bitbucket.org/fasthh/react-native-mobile-client.git master
```
Enter folder and install node modules
```sh
$ cd master
$ npm install
```
## iOS ##
### Simulator ###
Run on simulator (xCode needed)
```sh
$ react-native run-ios --simulator
```

### Running on Device ###
1. Open master/ios/fastApp/AppDelegate.m
2. Change the IP in the URL from localhost to your laptop's IP. On Mac, you can find the IP address in System Preferences / Network.
3. Go to master folder and run packager
```sh
$ npm start
```
4. In Xcode select your phone as build target and press "Build and run"

### Using offline bundle ###

When you run your app on device, we pack all the JavaScript code and the images used into the app's resources. This way you can test it without development server running and submit the app to the AppStore.

1.  Open master/ios/fastApp.xcworkspace
2. Change the scheme navigate to Product > Scheme > Edit Scheme... in xcode and change Build Configuration between Debug and Release.
3. Run or Archive App
4. If you archived and exported fastApp.ipa, drag it and move to itunes


## Android ##
### Preparation (without Android Studio) ###
1. Open a terminal window
2. Navigate into project's root
3. Make react-native aware of your Sdk location
```sh
$ touch android/local.properties && echo "sdk.dir=<your Sdk location" >> android/local.properties
```
### Virtual Device ###
1. Open the preferred AVD manager and launch a new Virtual Device
2. Build and run
```sh
$ react-native run-android
```
If you do need to reload the app, just press the menu key (which may vary depending on your Emulator) and click on "Reload JS"
### Real Device ###
#### Build ####
1. Close the Virutal Device instance to prevent adb conflicts and connect your device making sure it has USB debugging enabled
2. Make sure it is available through adb
```sh
$ adb devices
```
3. Enable port forwarding on your device in order to let it see the packager (which is binding at localhost:8081)
```sh
$ adb reverse tcp:8081 tcp:8081
```
4. Start the packager (if there isn't one already running)
```sh
$ react-native start
```
5. Build and run
```sh
$ react-native run-android
```
If you do need to reload the app, run `adb shell input keyevent 82` and, on the device, click on "Reload JS"
*Note*: Logs are available thourgh adb by running `adb logcat *:S ReactNative:V ReactNativeJS:V`
#### Use the apk ####
The .apk file is available in this repository under `release' folder